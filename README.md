Twig Content
============
`twig_content` is an extension to the TYPO3 CMS. It provides content elements which are rendered by the _twig_ engine and can
be used as an alternative or complement to `css_styled_content` or `fluid_styled_content`.


